//import klasy pomocniczej obsługującej wyjątki
import java.io.IOException;

//rozpoczęcie klasy kodu Java; najlepsze rozwiązanie - 1 plik, 1 klasa
public class Wejscie {
	//jezeli klasa ma byc także autonomicznym programem (czyli ma się uruchamiać jako program
	//MUSI posiadać poniżej wpisaną funkcję main 
	//funkcja może mieć postać jak poniżej zakomentowana (przyjmuje parametry z linni poleceń, dodanych przy uruchamianiu)
	//lub nic nie przyjmować (aktualnie aktywna)
	//public static void main(String[] args) {
	public static void main() {
		//domyślna metoda Java na wyświetlanie tekstu
		System.out.print("Podaj swoje imię: ");
		//inicjalizacja zmiennej byte, ktora może maksymalnie pomieścić 200 bajtów (20 znaków)
		byte[] b = new byte[200];
		//początek instrukcji wyjątku, Java wymaga, by w klazuli try znajdowała się funkcja read
		try {	
			//informujemy język, że chcemy by do zmiennej b wczytała to co na klawiaturze wpisze użytkownik
			System.in.read(b);
		}
		//ponieważ na razie nie zajmujemy sie obsługą wyjątków - po prostu dodajemy pustą obslugę ewentuanego błędu (nic sie nie stanie)
		catch(IOException ex) {
			
		}
		//tworzymy nową zmienną ciągu znakowego o nazwie i, do której przypisujemy ciąg znakowy utworzony z wczęśniej wczytanych bajtów
		String i = new String(b);
		////wyświetlamy w konsoli imię oraz jego długość (zawsze wyniesie tyle, ile posiada tablica b; w tym wypadku 200).
		//samo imie będzie również posiadać puste znaki (200 znaków).
		System.out.print("\nPodane przez Ciebie imię to " + i + 
				", posiada ono " + i.length() + " znaków.");
	}
}