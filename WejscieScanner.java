//musimy zaimportować obsługę klasy Scanner
import java.util.Scanner;

public class WejscieScnaner {
	public static void main(String[] args) {
		//tworzymy zmienną imie i inicjalizujemy ją wartością domyślną - pustą
		String imie = "";
		
		System.out.print("Podaj swoje imię: ");
		//tworzymy nową zmienną typu Scanner, podajmey by czytała wejście z konsoli systemowje (System.in)
		Scanner mojskan = new Scanner(System.in);
		//metoda next dostępna w Scanner pozwala na pobranie wszystkich wczytanych znaków w konsoli aż do znaku ENTER (on kończy wczytywanie)
		//rezultat wczytania przypisujemy do zmiennej imie
		imie =  mojskan.next();
		//zamykamy zmienną mojskan by zapobiec wewntualnym wyciekom danych
		mojskan.close();
		//jak poprzednio - wyświetlamuy imię oraz długość imienia; tym razem poprawną (wczytana tylko tyle znaków, ile wprowadził użytkownik)
		System.out.print("\nPodane przez Ciebie imię to " + imie + 
				", posiada ono " + imie.length() + " znaków.");
		
	}
}
